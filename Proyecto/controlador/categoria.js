$(function() { 
    var nom_icono;
    var nom_banner;
    
    subir_icono();    
  //  subir_banner();
    obtener_categorias();   
});


function obtener_categorias() {
	var accion_agregar = "<button type='button' class='btn btn-dark' " +
                        "onclick='agregar_categoria();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>";            

	var tabla = $('#tabla_categoria').dataTable({
		"columnDefs": [
		  {"title": "Nombre categoría", "targets": 0, "orderable": true, "className": "dt-body-center"},
		  {"title": "Ícono categoría", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Banner ", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar, "targets": 3, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "../language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

    $.ajax({
		url: '../modelo/acciones_categoria.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.salida_exitosa) {
				var data = response.datos;
			
				for(var i = 0; i < data.length; i++) {
					tabla.fnAddData([
					    data[i]["nombre_ciu"],
					    data[i]["ubicacion"],
					    data[i]["telefono"],
					    data[i]["correo_suc"],
					    data[i]["ima_mapa"],

    					"<button type='button' class='btn btn-warning btn-xs' onclick='editar(" + data[i]["id_sucursal"] + ");' title='Editar'>"+
	    				"<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
		    			"<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 " + 
                        "9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' " +
                        "d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 " +
					    "0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
					
					    "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar_sucursal(" + data[i]["telefono"] + ");' title='Eliminar'>"+
					    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
					    "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 " + 
                        ".5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
					    "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 " + 
                        "1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"
				    ]);
                }   
			} 

            else {
				swal('Error', response.mensaje[2], 'error');
			}      
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    }); 
}

function agregar_categoria() {
    $("#titulo-modal-categoria").html("Agregar Categorías"); 
    document.getElementById("form-categoria").reset();
    $("#btn-aceptar-categoria").attr("onClick", "agregar_categoriaBD()");
    $("#modal-categoria").modal("show");
}

function agregar_categoriaBD() {
    val_nombre = validarFormularioCategoria('#nom_categoria', "nombre");

	if (val_nombre == false){
         return false;
    }

    var nombre = $('#nom_categoria').val();
    
    /*$.ajax({
		dataType: 'json',
		async: true,
		url: '../modelo/acciones_categoria.php?accion=2',
		data: {direccion: direccion,
                telefono: telefono,
                correo: correo,
                archivo: nom_archivo,
                id_ciudad: id_ciudad},

		success: function (response) {    
		    if (response.salida_exitosa) {          
			    $("#modal-sucursal").modal("hide");
			    obtener_sucursales();
			  
		    }

            else {
			    swal('Error', response.mensaje[2], 'error');
		    }

		},
 
        error: function (e) {
		    swal('Error', e.responseText, 'error');
		}

    });*/
}

function subir_icono() {
    $('#iconoForm').on('submit', function(e) {
        console.log("Holaa");
        //e.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: '../modelo/acciones_subir_icono.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('#submitBtn1').attr("disabled","disabled");
                $('#iconoForm').css("opacity",".5");
            },
            success: function(response) { 
                $('#estado1').html('');
                
                if(response.estado == "ok") {
                    nom_archivo = response.nom_archivo;
                    $('#iconoForm')[0].reset();
                    $('#estado1').html('<p class="alert alert-success">'+response.mensaje + ': ' + response.nom_archivo + '</p>');
                }
                
                else{
                    $('#estado1').html('<p class="alert alert-danger">'+response.mensaje+'</p>');
                }
                
                $('#iconoForm').css("opacity","");
                $("#submitBtn1").removeAttr("disabled");
            }
        });
    });
}

function validarFormularioCategoria (id, nombre) {
    if ($(id).val().trim().length<1) {
	    swal('Atención',"No ha completado el campo " + nombre, 'info');
		return false;
    }
	  
    return true;
}
