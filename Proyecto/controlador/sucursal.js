$(function() { 
    var nom_archivo;
    var id_ciudad;

    subir_archivo();    
    obtener_sucursales();   
});


function obtener_sucursales() {
	var accion_agregar = "<button type='button' class='btn btn-dark' " +
                        "onclick='agregar_sucursal();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>";            

	var tabla = $('#tabla_sucursal').dataTable({
		"columnDefs": [
		  {"title": "Ciudad", "targets": 0, "orderable": true, "className": "dt-body-center"},
		  {"title": "Dirección", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Teléfono", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "Correo electrónico", "targets": 3, "orderable": true, "className": "dt-body-center"},
		  {"title": "Mapa", "targets": 4, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar, "targets": 5, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "../language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

    $.ajax({
		url: '../modelo/acciones_sucursal.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.salida_exitosa) {
				var data = response.datos;
			
				for(var i = 0; i < data.length; i++) {
					tabla.fnAddData([
					    data[i]["nombre_ciu"],
					    data[i]["ubicacion"],
					    data[i]["telefono"],
					    data[i]["correo_suc"],
					    data[i]["ima_mapa"],

    					"<button type='button' class='btn btn-warning btn-xs' onclick=\"editar_sucursal('" + data[i]["correo_suc"] + "')\"; title='Editar'>"+
	    				"<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
		    			"<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 " + 
                        "9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' " +
                        "d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 " +
					    "0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
					
					    "<button type='button' class='btn btn-danger btn-xs' onclick=\"eliminar_sucursal('" + data[i]["correo_suc"] + "\','" + data[i]["ima_mapa"] + "')\";' title='Eliminar'>"+
					    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
					    "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 " + 
                        ".5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
					    "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 " + 
                        "1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"
				    ]);
                }   
			} 

            else {
				swal('Error', response.mensaje[2], 'error');
			}      
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });
}

function obtener_ciudades(){
	$.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_sucursal.php',
        data: {accion: 6},
        success: function (response) {   
		    if (response.salida_exitosa) {
			    var items="";
				items ="<option> Seleccionar </option>";

			    $.each(response.datos,function(index,item) {
				    items+="<option>"+item["nombre"]+"</option>";
			        $("#ciudad").html(items);
			    });
            }
     
            else {
                swal('Error', response.mensaje[2], 'error');
            }
	    }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function obtener_id_ciudad(){
    $(document).on('change', '#ciudad', function(event) {
        nom_ciudad = $("#ciudad option:selected").text();
    
        if (nom_ciudad != "Seleccionar"){
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_registro.php?accion=3',
                data: {nombre: nom_ciudad},

                success: function (response) {    
                    if (response.salida_exitosa) {
    			        $.each(response.datos,function(index,item) {
                            id_ciudad = item["id_ciudad"]; 
                        });
                    } 

                    else {
                        swal('Error', response.mensaje[2], 'error');
                    }
                }, 

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }
    });
}

/* levanta el modal para ingresar datos */
function agregar_sucursal() {
    $("#titulo-modal-sucursal").html("Agregar sucursal"); 
    document.getElementById("form-sucursal").reset();
    obtener_ciudades();
    id_ciudad = obtener_id_ciudad();
    $("#btn-aceptar-sucursal").attr("onClick", "agregar_sucursalBD()");
    $("#modal-sucursal").modal("show");
}

function agregar_sucursalBD() {
    val_telefono = validarFormularioSucursal('#telefono', "teléfono");
    val_direccion = validarFormularioSucursal('#nom_direccion', "direccion");
    val_correo = validarFormularioSucursal('#correo', "correo");
    val_ciudad = validarFormularioSucursal('#ciudad', "ciudad");

	if (val_telefono == false || val_direccion == false || val_correo == false || val_ciudad == false){
         return false;
    }

    var direccion = $('#nom_direccion').val();
	var telefono = $('#telefono').val();
	var correo = $('#correo').val();
    
    $.ajax({
		dataType: 'json',
		async: true,
		url: '../modelo/acciones_sucursal.php?accion=2',
		data: {direccion: direccion,
                telefono: telefono,
                correo: correo,
                archivo: nom_archivo,
                id_ciudad: id_ciudad},

		success: function (response) {    
		    if (response.salida_exitosa) {          
			    $("#modal-sucursal").modal("hide");
			    obtener_sucursales();
			  
		    }

            else {
			    swal('Error', response.mensaje[2], 'error');
		    }

		},
 
        error: function (e) {
		    swal('Error', e.responseText, 'error');
		}

    });
}

function eliminar_sucursal(correo, archivo){    
    swal({
        title:"¿Seguro que deseas eliminar?",
        text: "Una vez eliminado, no podrás recuperarlo",
        icon: "error",
        dangerMode: true,
        buttons: true,
    })
        
    .then((willDelete) => {
        if (willDelete) {
            // Borrar archivo de la carpeta        
            borrar_archivo(archivo);

            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_sucursal.php',
                data: {accion: 3, correo: correo},
                success: function (response) {    
                    if (response.salida_exitosa) {
                        swal({
                            title: "Éxito",
                            text: "¡Se ha eliminado exitósamente!",
                            icon: "success",
                        });
                    
                        obtener_sucursales();
                    }
                    
                    else {
                        swal('Error', response.mensaje[2], 'error');
                    }
                },

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }

        else {
            swal({
                title: "Cancelado",
                text: "¡La eliminación se ha cancelado!", 
                icon: "info",
            });
        }
    });

}

function subir_archivo() {
    $("#fupForm").on('submit', function(e) {
        e.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: '../modelo/acciones_subir_sucursal.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('#submitBtn').attr("disabled","disabled");
                $('#fupForm').css("opacity",".5");
            },
            success: function(response) { 
                $('#estado').html('');
                
                if(response.estado == "ok") {
                    nom_archivo = response.nom_archivo;
                    $('#fupForm')[0].reset();
                    $('#estado').html('<p class="alert alert-success">'+response.mensaje + ': ' + response.nom_archivo + '</p>');
                }
                
                else{
                    $('#estado').html('<p class="alert alert-danger">'+response.mensaje+'</p>');
                }
                
                $('#fupForm').css("opacity","");
                $("#submitBtn").removeAttr("disabled");
            }
        });
    });
}

function borrar_archivo(archivo) {
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_sucursal.php?accion=7',
        data: {archivo: archivo},
        success: function (response) {    
            console.log(response);
        },

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioSucursal (id, nombre) {
    if ($(id).val().trim().length<1) {
	    swal('Atención',"No ha completado el campo " + nombre, 'info');
		return false;
    }
	  
    return true;
}

