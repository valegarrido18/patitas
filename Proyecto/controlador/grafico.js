$(document).ready(function() {
    
    Chart.plugins.register({
        beforeDraw: function(chartInstance) {
            var ctx = chartInstance.chart.ctx;
            ctx.fillStyle = "white";
            ctx.fillRect(0, 0, chartInstance.chart.width, chartInstance.chart.height);
        }
    });

    document.getElementById("download").addEventListener('click', function() {
        /*Get image of canvas element*/
        var canvas = document.getElementById("myChart");
        
        var url_base64 = canvas.toDataURL("image/jpeg", 1);
        
        /*get download button (tag: <a></a>) */
        var a =  document.getElementById("download");
        
        /*insert chart image url to download button (tag: <a></a>) */
        a.href = url_base64;
    });
    
    $.ajax({
	url: '../modelo/acciones_pago.php',
	data: {accion: 1},
	type: 'POST',
	dataType: 'json',
	async: true,
	success: function(response) {
		if (response.salida_exitosa) {
			var info = response.datos;
		
			var ctx = document.getElementById("myChart").getContext('2d');
			var myBarChart = new Chart(ctx, {
				type: 'bar',
				data: ["hola", "chao"],
				options: {
						scales: {
							xAxes: [{
								stacked: true
							}],
							yAxes: [{
								stacked: true
							}]
						}
					}
			});
		
		 
		} else {
			swal('Error', response.mensaje[2], 'error');
		}      
	}, 
	
	error: function(jqXHR, textStatus, errorThrown ) {
		swal('Error', textStatus + " " + errorThrown, 'error');
	}
	});
    

    
});
