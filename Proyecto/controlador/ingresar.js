$(function() {
  
  $("#formulario_ingreso").submit(function () {
    var correo = $('#correo').val().trim();
    var contrasena = $('#contraseña').val().trim();

    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {mail: correo, pwd: contrasena},
        url: "../lib/validar_contra.php",
        async: "false",
        success: 
            function (response) {
                if (response.salida_exitosa) {
                    window.location=response.ubicacion;
                }
 
                else {
					swal('Error', response.mensaje, 'error');
                }
            },
 
        /*error: 
            function (e) {
                logout();
            }*/
    });
    
    $('#corrreo').val('');
    $('#contrasena').val('');

    return false; 

    });
});
