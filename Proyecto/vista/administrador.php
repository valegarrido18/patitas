<?php
	require_once("../lib/compartido.php");
?>

<!DOCTYPE html> 
<html>
    
    <head>
        <title> Administración </title>
        <?php
			head();
		?>
    </head>

     
    <body style="background-color:#f2f2f2;">

		<!-- barra de arriba --> 
		<?php
			navbar();
		?>

		<!-- columnas --> 
		<div class="container p-5 my-5" align="center">
			<img align="center" src="./imagenes/patitas6.png" width="55%">
		</div>

		<!-- Card para productos --> 
		<div id="lista_servicio">
			<div class="container">
				<div class="card-deck">

					<!-- gestión de productos -->
					<div id="productos" class="card" align="center">
						<div class="text-center">
							<img src="./imagenes/22.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>

					    <div class="card-footer text-center">
						    <h5 class="card-title"> <strong> Gestión de productos </strong> </h5>
						</div>
					</div>
		
					<!-- gestión de tipo de pago -->
					<div id="pago" class="card" align="center">
						<div class="text-center">
							<img src="./imagenes/28.png" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>
		
						<div class="card-footer text-center">
							<h5 class="card-title"> <strong> Gestión de tipos de pagos </strong> </h5>
						</div>	
					</div>
		
					<!-- gestión de categorías -->
					<div id="categoria" class="card" align="center">
						<div class="text-center">
							<img src="./imagenes/24.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>
			
						<div class="card-footer text-center">
							<h5 class="card-title"> <strong> Gestión de categorías </strong> </h5>
						</div>
					</div>
		
					<!-- gestión de sucursales -->
					<div id="sucursal" class="card" align="center">
						<div class="text-center">
							<img src="./imagenes/26.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>
		
						<div class="card-footer text-center">
							<h5 class="card-title"> <strong> Gestión de sucursales </strong></h5>
						</div>
					</div>


					<!-- gestión de ciudades --> 
					<div id="ciudades" class="card" align="center">
						<div class="text-center">
							<img src="./imagenes/ciudad3.png" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>

					    <div class="card-footer text-center">
						    <h5 class="card-title"> <strong> Gestión de ciudades </strong> </h5>
						</div>
					</div>

				</div>
			</div>
		</div>
		
		<script>
			$(document).ready(function(){
				$(".tabla1").hide();
				$("#pago").click(function(){
					$(".tabla1").show();
					$(".tabla2").hide();	
					$(".tabla3").hide();
					$(".tabla4").hide();
				});	
			});
		</script>
		
        <br><br> 
		<div class="tabla1"> 
            <div class="col-lg-12">
                <div class="card">
                    <div id="titulo" class="card-header bg-secondary" style="color:white">
						Tabla gestión pago
                    </div> 

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabla_pago" class="display table compact nowrap"></table>
                       </div>
                    </div> 
                </div> 
            </div>
        </div>  
        
        <script>
			$(document).ready(function(){
				$(".tabla2").hide();
				$("#sucursal").click(function(){
					$(".tabla2").show();
					$(".tabla1").hide();
					$(".tabla3").hide();	
					$(".tabla4").hide();	
				});	
			});
		</script>

        <div class="tabla2"> 
            <div class="col-lg-12">
                <div class="card">
                    <div id="titulo" class="card-header bg-secondary" style="color:white">
						Tabla gestión sucursales
                    </div> 

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabla_sucursal" class="display table compact nowrap"></table>
                       </div>
                    </div> 
                </div> 
            </div>
        </div> 
        
        <script>
			$(document).ready(function(){
				$(".tabla3").hide();
				$("#categoria").click(function(){
					$(".tabla3").show();
					$(".tabla1").hide();	
					$(".tabla2").hide();
					$(".tabla4").hide();
				});	
			});
		</script>

        <div class="tabla3"> 
            <div class="col-lg-12">
                <div class="card">
                    <div id="titulo" class="card-header bg-secondary" style="color:white">
						Tabla gestión categorías
                    </div> 

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabla_categoria" class="display table compact nowrap"></table>
                       </div>
                    </div> 
                </div> 
            </div>
        </div> 


        <script>
			$(document).ready(function(){
				$(".tabla4").hide();
				$("#ciudades").click(function(){
					$(".tabla4").show();
					$(".tabla1").hide();	
					$(".tabla2").hide();
					$(".tabla3").hide();
				});	
			});
		</script>

        <div class="tabla4"> 
            <div class="col-lg-12">
                <div class="card">
                    <div id="titulo" class="card-header bg-secondary" style="color:white">
						Tabla gestión ciudades
                    </div> 

                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="tabla_ciudad" class="display table compact nowrap"></table>
                       </div>
                    </div> 
                </div> 
            </div>
        </div> 
        <br><br>
		       
        <!-- TABLA USUARIOS --> 
        <script>
			$(document).ready(function(){
				$("#btn_usuarios").click(function(){
					 $("#modal-usuario").modal("show");
				});	
			});
		</script>

        <!-- modals usuario-->
		<div class="modal" id="modal-usuario">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-usuario">Gestión de usuarios</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<div class="tabla5"> 
								<div class="col-lg-12">
									<div class="card">
										<div id="titulo" class="card-header bg-secondary" style="color:white">
											Tabla gestión usuarios
										</div> 

										<div class="card-body">
											<div class="table-responsive">
												<table id="tabla_usuario" class="display table compact nowrap"></table>
										   </div>
										</div> 
									</div> 
								</div>
							</div>  
						</div>
					</div>
				</div>
			</div>
		</div>

         <!-- modals registro-->
		<div class="modal" id="modal-registro">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-registro">Crear</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<form id="form-rut" role="form" method="post" enctype="multipart/form-data">
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<label for="rut">Rut:</label>
										<input type="text" class="form-control" id="rut" placeholder="Ingrese rut" name="rut" >
								</div>
							    <div class="clearfix"></div>
							</form>
			 
							<form id="form-registro" role="form" method="post" enctype="multipart/form-data" class="was-validated">
					 
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="usuario">Usuario:</label>
									<input type="text" class="form-control" id="usuario" placeholder="Ingrese nombre de usuario" name="usuario" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>

								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="email">Correo:</label>
									<input type="text" class="form-control" id="email" placeholder="Ingrese correo" name="email" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="contrasena">Contraseña:</label>
									<input type="password" class="form-control" id="contrasena" placeholder="Ingrese una contraseña" name="contrasena" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="confirmar">Confirmar contraseña:</label>
									<input type="password" class="form-control" id="confirmar" placeholder="Ingrese nuevamente la contraseña" name="confirmar" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="direccion">Dirección:</label>
									<input type="text" class="form-control" id="direccion" placeholder="Ingrese dirección" name="direccion" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>

								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<form action="/action_page.php">
										<div class="form-group">
											<label for="ciudad_registro">Ciudad:</label>
											<select class="form-control" id="ciudad_registro" name="ciudad_registro">
                                                <option value="" selected="seleccionado"></option>
											</select>
										</div>
									</form>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<form action="/action_page.php">
										<div class="form-group">
											<label for="perfil">Perfil:</label>
											<select class="form-control" id="perfil" name="perfil">
                                                <option value="" selected="seleccionado"></option>
											</select>
										</div>
									</form>
								</div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
					</div>
				
					<!-- footer modal -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" id="btn-aceptar-registro">Aceptar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
					</div>
				</div>
			</div>
		</div>

        <!-- modals especialidad-->
		<div class="modal" id="modal-especialidad">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-especialidad">Crear</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<form id="form-especialidad" role="form" method="post" enctype="multipart/form-data" class="was-validated">
					 
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="nom_espec">Nuevo pago:</label>
									<input type="text" class="form-control" id="nom_espec" placeholder="Ingrese nombre de pago" name="nom_espec" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
					  
								<div class="clearfix"></div>
							</form>
						</div>
					</div>
				
					<!-- footer modal -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" id="btn-aceptar-especialidad">Aceptar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
					</div>
				</div>
			</div>
		</div>
		
		<!-- modals sucursal-->
		<div class="modal" id="modal-sucursal">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-sucursal">Crear</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<form id="form-sucursal" role="form" method="post" enctype="multipart/form-data" class="was-validated">
		
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="nom_direccion">Dirección:</label>
									<input type="text" class="form-control" id="nom_direccion" placeholder="Ingrese direccion" name="nom_direccion" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="telefono">Teléfono:</label>
									<input type="text" class="form-control" id="telefono" placeholder="Ingrese teléfono" name="telefono" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="correo">Correo electrónico:</label>
									<input type="text" class="form-control" id="correo" placeholder="Ingrese correo electrónico" name="correo" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
							</form>
								<form>
									<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<form action="/action_page.php">
											<div class="form-group">
												<label for="ciudad">Ciudad:</label>
												<select class="form-control" id="ciudad" name="ciudad" required>
													<option>--Seleccionar--</option>
												</select>
											</div>
										</form>
									</div>
									
									<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<form id="fupForm" method="post" enctype="multipart/form-data">
											<div class="form-group">
												<label for="mapa">Agregar mapa:</label>
												<input type="file" name="mapa" class="form-control-file" id="mapa" required>
											</div>
                                            <p></p>
                                            <input type="submit" name="submit" class="btn btn-secondary" id="submitBtn" value="Subir archivo"/>
										</form>
									</div>
					                <span id="estado" name = "estado"></span>
								</form>
								<div class="clearfix"></div>
							
						</div>
					</div>
				
					<!-- footer modal -->
					<div class="modal-footer">
						<button type="submit" name="btn-aceptar-sucursal" class="btn btn-secondary" id="btn-aceptar-sucursal" value="Subir">Aceptar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- modals categorias-->
		<div class="modal" id="modal-categoria">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-categoria">Crear</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<form id="form-categoria" role="form" method="post" enctype="multipart/form-data" class="was-validated">
		
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="nom_categoria">Nombre categoría:</label>
									<input type="text" class="form-control" id="nom_categoria" placeholder="Ingrese una categoría" name="nom_categoria" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
							</form>
								<form>
									<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<form id="iconoForm" method="post" enctype="multipart/form-data">
											<div class="form-group">
												<label for="icono">Agregar ícono:</label>
												<input type="file" name="icono" class="form-control-file" id="icono" required>
											</div>
                                            <p></p>
                                            <input type="submit" name="submit" class="btn btn-secondary" id="submitBtn1" value="Subir archivo"/>
										</form>
									</div>
									<span id="estado1" name = "estado1"></span>
									
									<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<form id="bannerForm" method="post" enctype="multipart/form-data">
											<div class="form-group">
												<label for="banner">Agregar banner:</label>
												<input type="file" name="banner" class="form-control-file" id="banner" required>
											</div>
                                            <p></p>
                                            <input type="submit" name="submit" class="btn btn-secondary" id="submitBtn2" value="Subir archivo"/>
										</form>
									</div>
									
					                <span id="estado2" name = "estado2"></span>
								</form>
								<div class="clearfix"></div>
						</div>
					</div>
				
					<!-- footer modal -->
					<div class="modal-footer">
						<button type="submit" name="btn-aceptar-categoria" class="btn btn-secondary" id="btn-aceptar-categoria" value="Subir">Aceptar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
					</div>
				</div>
			</div>
		</div>
		
        <!-- modals ciudad-->
		<div class="modal" id="modal-ciudad">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-ciudad">Crear</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<form id="form-ciudad" role="form" method="post" enctype="multipart/form-data" class="was-validated">
					 
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="nom_ciudad">Nombre ciudad:</label>
									<input type="text" class="form-control" id="nom_ciudad" placeholder="Ingrese nombre de ciudad" name="nom_ciudad" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
					  
								<div class="clearfix"></div>
							</form>
						</div>
					</div>
				
					<!-- footer modal -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" id="btn-aceptar-ciudad">Aceptar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
					</div>
				</div>
			</div>
		</div>

        <br><br><br>

        <footer class="page-footer bg-dark" >
			<br><br><br><br>    
        </footer> 

		<script src="../controlador/tipo_pago.js"></script>
	    <script src="../controlador/sucursal.js"></script> 
	    <script src="../controlador/categoria.js"></script>
	    <script src="../controlador/ciudad.js"></script>
	    <script src="../controlador/usuarios.js"></script>
    </body> 
</html> 
