<?php
	require_once("../lib/compartido.php");
?>

<!DOCTYPE html> 
<html>

	<head>
		
		<title> Patitas Tienda de Mascotas </title>
		<?php
			head();
		?>
		
    </head>

    <body style="background-color:#f2f2f2;">

		<?php
			navbar();
		?>
     
		<!-- columnas --> 
		<div class="container p-5 my-5" align="center">
			<img align="center" src="../vista/imagenes/patitas6.png" width="55%">
		</div>

		<div class="container" align="center">
			<div class="row">
				<div class="col-sm-4">
					<img align="center" src="../vista/imagenes/perro.png" width="50%">
					<h3>Perros</h3>
				</div>

				<div class="col-sm-4">
					<img align="center" src="../vista/imagenes/gato.png" width="50%">
					<h3>Gatos</h3>
				</div>

				<div class="col-sm-4">
					<img align="center" src="../vista/imagenes/conejo.png" width="50%">
					<h3>Exóticos</h3>
				</div>
			</div>
		</div>

		<br><br><br><br>

		<!-- Card para productos --> 
		<div id="lista_servicio">
			<div class="container">
				<div class="titulo" align="center"><h4>¡Conoce nuestros productos!</h4>
			</div>
			<br>
			<br>
			
			<div class="card-deck">
				<div class="card" align="center">
					<div class="text-center">
						<img src="../vista/imagenes/4.webp" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
					</div>
					
					<div class="card-block">
						<h4 class="card-title" height="90" width="50">Saco de comida</h4>
						<p class="card-text">Comida para gato Saco 1kg.</p>
						<p class="card-text">$9.500</p>
						<h3 " "</h3>
					</div>
					
					<div class="card-footer text-center">
						<a href="#" class="btn btn-dark">Comprar</a>
					</div>
				</div>
				
				<div class="card" align="center">
					<div class="text-center">
						<img src="../vista/imagenes/1.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
					</div>
					
					<div class="card-block">
						<h4 class="card-title">Arena de gato</h4>
						<p class="card-text">Arena para gatos cristales 1.6 kg.</p>
						<p class="card-text">$6.990</p>
						<h3 " "</h3>
					</div>
					
					<div class="card-footer text-center">
						<a href="#" class="btn btn-dark">Comprar</a>
					</div>
				</div>
				
				<div class="card" align="center">
					<div class="text-center">
						<img src="../vista/imagenes/2.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
					</div>
					
					<div class="card-block">
						<h4 class="card-title">Saco de comida</h4>
						<p class="card-text">Comida para perro cachorro saco 18 kg.</p>
						<p class="card-text">$18.500</p>
						<h3 " "</h3>
					</div>
					
					<div class="card-footer text-center">
						<a href="#" class="btn btn-dark">Comprar</a>
					</div>
				</div>
			</div>
		</div>
		
		<br>
		
			<div class="container">
				<div class="card-deck">
					<div class="card" align="center">
						<div class="text-center">
							<img src="../vista/imagenes/5.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>
						
						<div class="card-block">
							<h4 class="card-title" height="90" width="50">Cama de mascota</h4>
							<p class="card-text">Cama para perro mediana</p>
							<p class="card-text">$20.000</p>
							<h3 " "</h3>
						</div>
						
						<div class="card-footer text-center">
							<a href="#" class="btn btn-dark">Comprar</a>
						</div>
					</div>
			
					<div class="card" align="center">
						<div class="text-center">
							<img src="../vista/imagenes/6.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>
						
						<div class="card-block">
							<h4 class="card-title">Comida roedores</h4>
							<p class="card-text">Comida para hamster y gerbos bolsa 500 gr.</p>
							<p class="card-text">$1.200</p>
							<h3 " "</h3>
						</div>
						
						<div class="card-footer text-center">
							<a href="#" class="btn btn-dark">Comprar</a>
						</div>
					</div>
					
					<div class="card" align="center">
						<div class="text-center">
							<img src="../vista/imagenes/7.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
						</div>
					
						<div class="card-block">
							<h4 class="card-title">Comida de conejo</h4>
							<p class="card-text">Comida para conejo adulto 300 gr</p>
							<p class="card-text">$5.400</p>
							<h3 " "</h3>
						</div>
						
						<div class="card-footer text-center">
							<a href="#" class="btn btn-dark">Comprar</a>
						</div>
					</div>
				</div>
			</div>
			
		<br>
	
		<div class="container">
			<div class="card-deck">
				<div class="card" align="center">
					<div class="text-center">
						<img src="../vista/imagenes/8.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
					</div>
					
					<div class="card-block">
						<h4 class="card-title" height="90" width="50">Tarro de comida</h4>
						<p class="card-text">Comida para tortuga 250 ml </p>
						<p class="card-text">$7.990</p>
						<h3 " "</h3>
					</div>
					
					<div class="card-footer text-center">
						<a href="#" class="btn btn-dark">Comprar</a>
					</div>
				</div>
				
				<div class="card" align="center">
					<div class="text-center">
						<img src="../vista/imagenes/9.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
					</div>
					
					<div class="card-block">
						<h4 class="card-title">Juguete de mascota</h4>
						<p class="card-text">Pelota para perros de vinilo</p>
						<p class="card-text">$1.160</p>
						<h3 " "</h3>
					</div>
					
					<div class="card-footer text-center">
						<a href="#" class="btn btn-dark">Comprar</a>
					</div>
				</div>
				
				<div class="card" align="center">
					<div class="text-center">
						<img src="../vista/imagenes/10.jpeg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
					</div>
					
					<div class="card-block">
						<h4 class="card-title">Rascador de gato</h4>
						<p class="card-text">Rascador gato con juguete de 41 cm</p>
						<p class="card-text">$28.900</p>
						<h3 " "</h3>
					</div>
					
					<div class="card-footer text-center">
						<a href="#" class="btn btn-dark">Comprar</a>
					</div>
				</div>
			</div>
		</div>
		
		<br>
		<br>
		<br>
		
		<footer class="page-footer bg-dark" >
			<div class="container" align="center">
				<br><br>
				<FONT COLOR="white"><h5 class="white-text">Si tienes alguna duda, consulta o sugerencia no dudes en llamarnos al 222266339 o escríbenos al mail contacto@patitas.cl y nos contactaremos contigo a la brevedad. <br> <a class="grey-text text-lighten-3" href="#!">Conócenos</a></h5></FONT>
			</div> <br><br>
			
			<!--<script src="../controlador/compartido.js"></script> -->
			
        </footer> 
        
    </body>
    
    <?php
		pushbar();
	?>
     
</html> 
