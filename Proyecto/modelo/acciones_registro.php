<?php

require_once("../lib/compartido.php");

if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1: 
      insertar($conn);
      break;  

    case 2: 
      seleccionar_ciudad($conn);
      break;
 
     case 3: 
      obtener_id($conn);
      break;
	}
}

function insertar($conn) {
    $rut = $_REQUEST['rut'];
    $usuario = $_REQUEST['usuario'];
    $email = $_REQUEST['email'];
    $contrasena = $_REQUEST['contrasena'];
    $direccion = $_REQUEST['direccion'];
    $id_ciudad = $_REQUEST['id_ciudad'];
    
    $encriptar_contra = encriptar($contrasena);

    $sql = "insert into usuario (correo, id_perfil, id_ciudad, rut, nombre, contrasena, direccion) values (:email, 2, :id_ciudad, :rut, :nombre, :contrasena, :direccion) ";

    $stmt = $conn->prepare($sql);

    $stmt->bindValue(':email', $email); 
    $stmt->bindValue(':id_ciudad', $id_ciudad); 
    $stmt->bindValue(':rut', $rut); 
    $stmt->bindValue(':nombre', $usuario); 
    $stmt->bindValue(':contrasena', $encriptar_contra); 
    $stmt->bindValue(':direccion', $direccion); 

    $res = ejecutarSQL($stmt);
  
     echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function seleccionar_ciudad($conn) {
    $sql= "select id_ciudad, nombre from ciudad;";
	
    $stmt = $conn->prepare($sql);
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function obtener_id($conn) {
    $nom_ciudad = $_REQUEST['nombre'];

    $sql= "select id_ciudad from ciudad where nombre = :nom_ciudad;";
	
    $stmt = $conn->prepare($sql);

    $stmt->bindValue(':nom_ciudad', $nom_ciudad); 
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}
