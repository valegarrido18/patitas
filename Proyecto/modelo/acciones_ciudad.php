<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();

    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                # select 
                seleccionar($conn);
                break;  
            case 2:
                # insertar
                insertar($conn);
                break;
	        case 3:
                # delete
                eliminar($conn);
                break;
	        case 4:
                # select where = ?
                seleccionarUno ($conn);
                break;
            case 5:
                # update
                actualizar($conn);
                break;
        }  
    }

function seleccionar ($conn) {
    $sql= "select * from ciudad";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function insertar($conn) {
    $nombre = $_REQUEST['nombre'];

    $sql = "insert into ciudad(nombre) values(:nombre)";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nombre', $nombre);
  
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function eliminar($conn) {
  $id_ciudad = $_REQUEST['id_ciudad'];

  $sql = "delete from ciudad where id_ciudad = :id_ciudad";

  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_ciudad', $id_ciudad);
  $res = ejecutarSQL($stmt);
  
  echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function actualizar ($conn) {
  $id_ciudad = $_REQUEST['id_ciudad'];
  $nombre = $_REQUEST['nombre'];
 
  $sql = "update ciudad set nombre = :nombre where id_ciudad = :id_ciudad";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_ciudad', $id_ciudad);
  $stmt->bindValue(':nombre', $nombre);

  $res = ejecutarSQL($stmt);  
  echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function seleccionarUno ($conn) {
  $id_ciudad = $_REQUEST['id_ciudad'];
  $sql= "select nombre from ciudad where id_ciudad = :id_ciudad";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_ciudad', $id_ciudad);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}
?>
