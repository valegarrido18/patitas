<?php
session_start();
require_once("../lib/compartido.php");
validarSesion();


if (isset($_REQUEST['accion'])) {
  $conn = conectarBD();
  
  switch ($_REQUEST['accion']) {
    case 1:
      # select 
      seleccionar($conn);
      break;  

    case 2:
      # insertar
      insertar($conn);
      insertar_ubicacion($conn);
      break;

	case 3:
      # delete
      eliminar($conn);
      break;

	case 4:
      $conn = conectarBD();
      seleccionarUno ($conn);
      break;

    case 5:
      # update
      actualizar($conn);
      break;

	case 6: 
      seleccionar_ciudad($conn);
      break; 
  }  
}

function seleccionar ($conn) {
    $sql= " select nombre, ima_categoria, ima_banner from mascota;";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}
