<?php

	if (session_status() == PHP_SESSION_NONE) {
	  session_start();
	}

	setlocale (LC_TIME, "es_ES.utf8");
	
	$file = "../conf.ini";
	$ini = parse_ini_file($file, false);

	function validarSesion() {
		if (isset($_SESSION["registro_patitas"])) {
			if ($_SESSION["registro_patitas"] != true) {
				logout();
			}
		} 
		
		else {
			logout();
		}
	}
	
	function logout() {
		header('Location: ../lib/cerrar_sesion.php');
	}
	
	function encriptar($string) {
		$encrypt_method = $GLOBALS['ini']['encrypt_method'];
		$secret_key = $GLOBALS['ini']['secret_key'];
		$secret_iv = $GLOBALS['ini']['secret_iv'];
		$encrypted = openssl_encrypt($string, $encrypt_method, $secret_key, 0, $secret_iv);
		$encrypted = base64_encode($encrypted);
		return $encrypted;
	}

	function desencriptar($string) {
		$encrypt_method = $GLOBALS['ini']['encrypt_method'];
		$secret_key = $GLOBALS['ini']['secret_key'];
		$secret_iv = $GLOBALS['ini']['secret_iv'];
		$decrypted = openssl_decrypt(base64_decode($string), $encrypt_method, $secret_key, 0, $secret_iv);
		return $decrypted;
	}
	
	function conectarBD () {
		$i = $GLOBALS['ini'];
		$host = $i["host"];
		$db = $i["db"];
		$user = $i["user"]; 
		$password = $i["password"];
		$port = $i["port"];
  
		// conectar a la base de datos
		try {
			$conn = new PDO('pgsql:host='.$host.';port='.$port.';dbname='.$db.';user='.$user.';password='.$password);
		
			return $conn;
		} 
			
		catch (PDOException $e) {
			//echo $e->getMessage();
			return false;
		}
	}
	
	function ejecutarSQL ($stmt) {
		$res = array();
		$res["salida_exitosa"] = false;
		$res["mensaje"] = "Error SQL";
		$res["datos"] = null;

		try {
			if ($stmt->execute()) {
				$res["datos"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
				$res["mensaje"] = "éxito";
				$res["salida_exitosa"] = true;
      
			} else {
      
				/* https://www.postgresql.org/docs/12/errcodes-appendix.html */
				$arrayError['23503'] = "El registro no se puede eliminar porque tiene información asociada.";

				$res["mensaje"] = "Error SQL, Contacte al soporte";      
			}

		} catch (PDOException $e) {
			$res["mensaje"] = $e->getMessage();
		}

		return $res;
	}
	
	
	function head () {
		
		$str = <<<EOF
		
		<meta charset="utf-8">
		<link rel="icon" href="../vista/imagenes/huella.png">
	   
		<meta name="viewport" content="width=device-width, initial-scale=1">
	    
		<!-- carga archivo css -->
		<link rel="stylesheet" href="../css/estilos.css">
        <link rel="stylesheet" href="../css/pushbar.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

		<!-- carga archivos javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        
		<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
		
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
EOF;

echo $str;
	}
	
	function pushbar () {
		
		$carrito = <<<EOF
		
		<script src="../controlador/pushbar.js"></script>
		<script>
			var pushbar = new Pushbar({
				blur: true,
				overlay: true
			});
		</script>
EOF;
echo $carrito;
	}
	
	function navbar () {
		
		$inicio = <<<EOF
		
		<!-- barra de arriba --> 
		<nav class="navbar navbar-dark bg-dark" >
			<!-- casa -->  
			<a href="../index/patitas.php" class="navbar-brand" >
				<svg xmlns="http://www.w3.org/2000/svg" width="55" height="40" fill="currentColor" class="bi bi-house" viewBox="0 0 16 16">
				<path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
				<path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
				</svg> 
			</a>
		
			<!-- lado izquierdo -->
			<form class="form-inline">
				<div class="input-group mb-0.5">
					<input type="text" class="form-control" placeholder="Buscar" aria-label="Buscar" aria-describedby="basic-addon2">
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="button"> 
							<!-- lupa -->
							  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" class="bi bi-search" viewBox="0 0 16 16">
							  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
							  </svg>
						</button> 
					</div>
				</div> 

				<!-- carrito -->
				<button class="btn-menu" data-pushbar-target="pushbar-menu"> <svg xmlns="http://www.w3.org/2000/svg" width="55" height="35" fill="white" class="bi bi-cart4" viewBox="0 0 16 16">
					<path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
					</svg>
				</button> 
		 
				<!-- barra carrito -->
				<div data-pushbar-id="pushbar-menu" data-pushbar-direction="left" class="pushbar">					
					<nav class="menu" align="center">
						<div class="container" align="center">
							<a class="tab-text"> <h5> Carrito de compras </h5><i class="fi-rr-user"></i> </a>
						</div>
						
						<div class="row g-0">
							<div class="col-md-7">
								<img src="../vista/imagenes/6.jpg" alt="..." class="img-fluid"/>
							</div>
							
							<div class="col-md-5">
								<div class="card-body">
									<small align="left">Comida roedores</small>
									<small align="left">$1.200</small>
								</div>
							</div>		
						</div> 	
					</nav>	
					
					<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					<div class="container" align="center">
						<button type="submit" class="btn btn-secondary btn-block">Comprar</button>
					</div>	  
				</div>	
									
				<!-- usuario -->
				<a href="../vista/ingreso.php" data-target="#ingreso"><span class="glyphicon glyphicon-log-in"></span> 
					<svg xmlns="http://www.w3.org/2000/svg" width="55" height="35" fill="white" class="bi bi-person-circle" viewBox="0 0 16 16">
						<path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
						<path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
					</svg>
				</a>
			</form>
		</nav>
	
EOF;
	$admin= <<<EOF
	
		<nav class="navbar navbar-dark bg-dark">
		<div class="container-fluid" align="left">
		
			<!-- casa -->  
			<a href="../index/patitas.php" class="navbar-brand" >
				<svg xmlns="http://www.w3.org/2000/svg" width="55" height="40" fill="currentColor" class="bi bi-house" viewBox="0 0 16 16">
					<path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
					<path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
				</svg> 
			</a>
			
			<ul class="nav navbar-nav  mr-auto">
				<li id="active_admin" class="dropdown">
				   <a href="../vista/administrador.php" <title style="color:white" align="left"> Administración</title> </a>  
				   <a id="btn_usuarios" style="color:white" >Usuarios </a>
				</li>
			  </ul>
				 	
			<!-- lado izquierdo -->
			<form class="form-inline">
			
			<!-- grafico -->
			<a href="../vista/grafico.php" class="btn-menu" data-pushbar-target="pushbar-menu"> <svg xmlns="http://www.w3.org/2000/svg" width="45" height="30" fill="white" class="bi bi-bar-chart-line-fill" viewBox="0 0 16 16">
				<path d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1V2z"/>
				</svg>
			</a> 
			
				<!-- usuario -->
				<div class="dropdown">
				  <a style="color:white" class="dropdown-toggle" data-toggle="dropdown" href="#"> 
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
							<path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
						</svg>{$_SESSION['nombre']}<span class="caret"></span></a>
				  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="background-color:#333333">
					<li>
						<a style="color:white" href="../lib/cerrar_sesion.php">Cerrar </a> 
					</li>
				  </ul>
				</div>	
			</form>
		</div>
		</nav>
EOF;

		$cliente = <<<EOF
		
		<!-- barra de arriba --> 
		<nav class="navbar navbar-dark bg-dark" >
			<!-- casa -->  
			<a href="../index/patitas.php" class="navbar-brand" >
				<svg xmlns="http://www.w3.org/2000/svg" width="55" height="40" fill="currentColor" class="bi bi-house" viewBox="0 0 16 16">
				<path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
				<path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
				</svg> 
			</a>
		
			<!-- lado izquierdo -->
			<form class="form-inline">
				<div class="input-group mb-0.5">
					<input type="text" class="form-control" placeholder="Buscar" aria-label="Buscar" aria-describedby="basic-addon2">
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="button"> 
							<!-- lupa -->
							  <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" class="bi bi-search" viewBox="0 0 16 16">
							  <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
							  </svg>
						</button> 
					</div>
				</div> 

				<!-- carrito -->
				<button class="btn-menu" data-pushbar-target="pushbar-menu"> <svg xmlns="http://www.w3.org/2000/svg" width="55" height="35" fill="white" class="bi bi-cart4" viewBox="0 0 16 16">
					<path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
					</svg>
				</button> 
		 
				<!-- barra carrito -->
				<div data-pushbar-id="pushbar-menu" data-pushbar-direction="left" class="pushbar">					
					<nav class="menu" align="center">
						<div class="container" align="center">
							<a class="tab-text"> <h5> Carrito de compras </h5><i class="fi-rr-user"></i> </a>
						</div>
						
						<div class="row g-0">
							<div class="col-md-7">
								<img src="../vista/imagenes/6.jpg" alt="..." class="img-fluid"/>
							</div>
							
							<div class="col-md-5">
								<div class="card-body">
									<small align="left">Comida roedores</small>
									<small align="left">$1.200</small>
								</div>
							</div>		
						</div> 	
					</nav>	
					
					<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

					<div class="container" align="center">
						<button type="submit" class="btn btn-secondary btn-block">Comprar</button>
					</div>	  
				</div>	

				<!-- usuario -->
				<div class="dropdown">
				  <a style="color:white" class="dropdown-toggle" data-toggle="dropdown" href="#"> 
						<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
							<path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
						</svg>{$_SESSION['nombre']}<span class="caret"></span></a>
				  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="background-color:#333333">
					<li><a style="color:white" href="../lib/cerrar_sesion.php">Cerrar </a></li>
				  </ul>
				</div>	
			</form>
		</nav>
									
EOF;
	
	if ($_SESSION['registro_patitas'] == true) {
		if ($_SESSION['perfil'] == 'administrador') {
			echo $admin;
		}
		else {
			echo $cliente; 
		}
	} 
	
		
	else {
		echo $inicio; 
	}
}
?>

